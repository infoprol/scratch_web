# This file is responsible for configuring your application
# and its dependencies with the aid of the Mix.Config module.
#
# This configuration file is loaded before any dependency and
# is restricted to this project.

# General application configuration
use Mix.Config

config :scratch_web,
  ecto_repos: [ScratchWeb.Repo]

# Configures the endpoint
config :scratch_web, ScratchWebWeb.Endpoint,
  url: [host: "localhost"],
  secret_key_base: "OzlcPYC4Z6M/ccCcR9ERS6P11ra8jCiXi2VE8w8SKGWCTONtG2zhKMKzPziHUFXL",
  render_errors: [view: ScratchWebWeb.ErrorView, accepts: ~w(html json), layout: false],
  pubsub_server: ScratchWeb.PubSub,
  live_view: [signing_salt: "OSFasRXz"]

# Configures Elixir's Logger
config :logger, :console,
  format: "$time $metadata[$level] $message\n",
  metadata: [:request_id]

# Use Jason for JSON parsing in Phoenix
config :phoenix, :json_library, Jason

# Import environment specific config. This must remain at the bottom
# of this file so it overrides the configuration defined above.
import_config "#{Mix.env()}.exs"
