defmodule ScratchWeb.Notebook do
  use Ecto.Schema
  import Ecto.Changeset

  schema "notebooks" do
    field :content, :string
    field :title, :string
    timestamps()

    belongs_to :users, ScratchWeb.User
  end

  @doc false
  def changeset(notebook, attrs) do
    notebook
    |> cast(attrs, [:title, :content])
    |> validate_required([:title])
  end
end
