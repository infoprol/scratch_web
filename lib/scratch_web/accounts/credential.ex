defmodule ScratchWeb.Accounts.Credential do
  use Ecto.Schema
  import Ecto.Changeset
  alias ScratchWeb.Accounts.User
  alias ScratchWeb.Accounts

  schema "credentials" do
    field :email, :string
    field :password, :string, virtual: true
    field :hashed_password, :string
    timestamps()

    belongs_to :user, User
  end

  @doc false
  def changeset(credential, attrs) do
    credential
    |> cast(attrs, [:email, :hashed_password])
    |> validate_required([:email, :hashed_password])
    |> unique_constraint(:email)
  end

  def changeset_with_password(cred, params \\ %{}) do
    cred
    |> cast(params, [:password])
    |> validate_required(:password)
    |> validate_length(:password, min: 5)
    |> validate_confirmation(:password, required: true)
    |> IO.inspect()
    |> hash_password()
    |> IO.inspect()
    |> changeset(params)
  end


  def hash_password(
      %Ecto.Changeset{
        changes: %{password: password}} = cs), do:
    cs |> put_change(:hashed_password, Accounts.hash_password(password))
  def hash_password(cs), do: cs

end
