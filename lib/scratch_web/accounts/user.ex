defmodule ScratchWeb.Accounts.User do
  use Ecto.Schema
  import Ecto.Changeset
  alias ScratchWeb.Accounts.Credential

  schema "users" do
    field :name, :string
    field :username, :string
    field :password, :string, virtual: true
    timestamps()

    has_one :credential, Credential
  end

  @doc false
  def changeset(user, attrs) do
    user
    |> cast(attrs, [:name, :username])
    |> validate_required([:username])
    |> unique_constraint(:username)
  end
end
