defmodule ScratchWeb.Repo do
  use Ecto.Repo,
    otp_app: :scratch_web,
    adapter: Ecto.Adapters.Postgres
end
