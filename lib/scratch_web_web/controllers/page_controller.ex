defmodule ScratchWebWeb.PageController do
  use ScratchWebWeb, :controller

  def index(conn, _params) do
    render(conn, "index.html")
  end
end
