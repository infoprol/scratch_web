defmodule ScratchWebWeb.SessionController do
  use ScratchWebWeb, :controller

  alias ScratchWeb.Accounts

  def new(conn, _), do: render(conn, "new.html")
  def delete(conn, _), do:
    conn |> configure_session(drop: true) |> redirect(to: "/")

  def create(conn, %{ "username" => username, "password" => password }) do
    IO.puts("sesh.create.username: #{username}, password: #{password}")
    case Accounts.check_login(username, password) do
      nil->
        conn
          |> put_flash(:error, "intruder alert!  the government has been notified!")
          |> redirect(to: Routes.session_path(conn, :new))
      user ->
        conn
          |> put_flash(:info, "you again? ...")
          |> put_session(:current_user, user)
          |> configure_session(renew: true)
          |> redirect(to: Routes.user_path(conn, :index))

    end
  end
  def create(conn, x) do
    IO.inspect(%{ "x" => x })
    conn |> halt()
  end


end
