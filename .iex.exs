
alias ScratchWeb.Accounts.Credential, as: C
alias ScratchWeb.Accounts.User, as: U
alias ScratchWeb.Accounts, as: A
alias ScratchWeb.Repo, as: Repo

new_params = %{
    "email" => "this@that.io",
    "password" => "password",
    "password_confirmation" => "password",
    "name" => "ford prefect",
    "username" => "geezus"}


import Ecto.Query

defmodule X do
  def qry_for(username) do
    qry =
        from u in U,
        join: c in assoc(u, :credential),
        where: u.username == ^username,
        select: u.id

  end
end
